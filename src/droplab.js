require('./window')(function(w){
  module.exports = function(deps) {
    deps = deps || {};
    var window = deps.window || w;
    var document = deps.document || window.document;
    var CustomEvent = deps.CustomEvent || require('./custom_event_polyfill');
    var HookButton = deps.HookButton || require('./hook_button');
    var HookInput = deps.HookInput || require('./hook_input');
    var utils = deps.utils || require('./utils');
    var DATA_TRIGGER = require('./constants').DATA_TRIGGER;

    var DropLab = function(hook){
      if (!(this instanceof DropLab)) return new DropLab(hook);
      this.ready = false;
      this.hooks = [];
      this.queuedData = [];
      this.config = {};
      this.loadWrapper;
      if(typeof hook !== 'undefined'){
        this.addHook(hook);
      }
    };


    Object.assign(DropLab.prototype, {
      load: function() {
        this.loadWrapper();
      },

      loadWrapper: function(){
        var dropdownTriggers = [].slice.apply(document.querySelectorAll('['+DATA_TRIGGER+']'));
        this.addHooks(dropdownTriggers).init();
      },

      addData: function () {
        var args = [].slice.apply(arguments);
        this.applyArgs(args, '_addData');
      },

      setData: function() {
        var args = [].slice.apply(arguments);
        this.applyArgs(args, '_setData');
      },

      destroy: function() {
        for(var i = 0; i < this.hooks.length; i++) {
          this.hooks[i].destroy();
        }
        this.hooks = [];
        this.removeEvents();
      },

      applyArgs: function(args, methodName) {
        if(this.ready) {
          this[methodName].apply(this, args);
        } else {
          this.queuedData = this.queuedData || [];
          this.queuedData.push(args);
        }
      },

      _addData: function(trigger, data) {
        this._processData(trigger, data, 'addData');
      },

      _setData: function(trigger, data) {
        this._processData(trigger, data, 'setData');
      },

      _processData: function(trigger, data, methodName) {
        for(var i = 0; i < this.hooks.length; i++) {
          var hook = this.hooks[i];
          if(hook.trigger.dataset.hasOwnProperty('id')) {
            if(hook.trigger.dataset.id === trigger) {
              hook.list[methodName](data);
            }
          }
        }
      },

      addEvents: function() {
        var self = this;
        this.windowClickedWrapper = function(e){
          var thisTag = e.target;
          if(thisTag.tagName !== 'UL'){
            // climb up the tree to find the UL
            thisTag = utils.closest(thisTag, 'UL');
          }
          if(utils.isDropDownParts(thisTag)){ return }
          if(utils.isDropDownParts(e.target)){ return }
          for(var i = 0; i < self.hooks.length; i++) {
            self.hooks[i].list.hide();
          }
        }.bind(this);
        document.addEventListener('click', this.windowClickedWrapper);
      },

      removeEvents: function(){
        w.removeEventListener('click', this.windowClickedWrapper);
        w.removeEventListener('load', this.loadWrapper);
      },

      changeHookList: function(trigger, list, plugins, config) {
        trigger = document.querySelector('[data-id="'+trigger+'"]');
        // list = document.querySelector(list);
        this.hooks.every(function(hook, i) {
          if(hook.trigger === trigger) {
            hook.destroy();
            this.hooks.splice(i, 1);
            this.addHook(trigger, list, plugins, config);
            return false;
          }
          return true
        }.bind(this));
      },

      addHook: function(hook, list, plugins, config) {
        if(!(hook instanceof HTMLElement) && typeof hook === 'string'){
          hook = document.querySelector(hook);
        }
        if(!list){
          list = document.querySelector(hook.dataset[utils.toDataCamelCase(DATA_TRIGGER)]);
        }

        if(hook) {
          if(hook.tagName === 'A' || hook.tagName === 'BUTTON') {
            this.hooks.push(new HookButton(hook, list, plugins, config));
          } else if(hook.tagName === 'INPUT') {
            this.hooks.push(new HookInput(hook, list, plugins, config));
          }
        }
        return this;
      },

      addHooks: function(hooks, plugins, config) {
        for(var i = 0; i < hooks.length; i++) {
          var hook = hooks[i];
          this.addHook(hook, null, plugins, config);
        }
        return this;
      },

      setConfig: function(obj){
        this.config = obj;
      },

      init: function () {
        this.addEvents();
        var readyEvent = new CustomEvent('ready.dl', {
          detail: {
            dropdown: this,
          },
        });
        window.dispatchEvent(readyEvent);
        this.ready = true;
        for(var i = 0; i < this.queuedData.length; i++) {
          this.addData.apply(this, this.queuedData[i]);
        }
        this.queuedData = [];
        return this;
      },
    });

    return DropLab;
  };
});
