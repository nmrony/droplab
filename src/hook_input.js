var CustomEvent = require('./custom_event_polyfill');
var Hook = require('./hook');

var HookInput = function(trigger, list, plugins, config) {
  Hook.call(this, trigger, list, plugins, config);
  this.type = 'input';
  this.event = 'input';
  this.addPlugins();
  this.addEvents();
};

Object.assign(HookInput.prototype, {
  addPlugins: function() {
    var self = this;
    for(var i = 0; i < this.plugins.length; i++) {
      this.plugins[i].init(self);
    }
  },

  addEvents: function(){
    var self = this;

    this.mousedown = function mousedown(e) {
      if(self.hasRemovedEvents) return;

      var mouseEvent = new CustomEvent('mousedown.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(mouseEvent);
    }

    this.input = function input(e) {
      if(self.hasRemovedEvents) return;

      var inputEvent = new CustomEvent('input.dl', {
        detail: {
          hook: self,
          text: e.target.value,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(inputEvent);
      self.list.show();
    }

    this.keyup = function keyup(e) {
      if(self.hasRemovedEvents) return;

      keyEvent(e, 'keyup.dl');
    }

    this.keydown = function keydown(e) {
      if(self.hasRemovedEvents) return;

      keyEvent(e, 'keydown.dl');
    }

    function keyEvent(e, keyEventName){
      var keyEvent = new CustomEvent(keyEventName, {
        detail: {
          hook: self,
          text: e.target.value,
          which: e.which,
          key: e.key,
        },
        bubbles: true,
        cancelable: true
      });
      e.target.dispatchEvent(keyEvent);
      self.list.show();
    }

    this.events = this.events || {};
    this.events.mousedown = this.mousedown;
    this.events.input = this.input;
    this.events.keyup = this.keyup;
    this.events.keydown = this.keydown;
    this.trigger.addEventListener('mousedown', this.mousedown);
    this.trigger.addEventListener('input', this.input);
    this.trigger.addEventListener('keyup', this.keyup);
    this.trigger.addEventListener('keydown', this.keydown);
  },

  removeEvents: function() {
    this.hasRemovedEvents = true;
    this.trigger.removeEventListener('mousedown', this.mousedown);
    this.trigger.removeEventListener('input', this.input);
    this.trigger.removeEventListener('keyup', this.keyup);
    this.trigger.removeEventListener('keydown', this.keydown);
  },

  restoreInitialState: function() {
    this.list.list.innerHTML = this.list.initialState;
  },

  removePlugins: function() {
    for(var i = 0; i < this.plugins.length; i++) {
      this.plugins[i].destroy();
    }
  },

  destroy: function() {
    this.restoreInitialState();
    this.removeEvents();
    this.removePlugins();
    this.list.destroy();
  }
});

module.exports = HookInput;
