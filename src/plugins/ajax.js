/* global droplab */
require('../window')(function(w){
  function droplabAjaxException(message) {
    this.message = message;
  }

  w.droplabAjax = {
    _loadUrlData: function _loadUrlData(url) {
      return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest;
        xhr.open('GET', url, true);
        xhr.onreadystatechange = function () {
          if(xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
              var data = JSON.parse(xhr.responseText);
              return resolve(data);
            } else {
              return reject([xhr.responseText, xhr.status]);
            }
          }
        };
        xhr.send();
      });
    },

    init: function init(hook) {
      var self = this;
      var config = hook.config.droplabAjax;

      if (!config || !config.endpoint || !config.method) {
        return;
      }

      if (config.method !== 'setData' && config.method !== 'addData') {
        return;
      }

      if (config.loadingTemplate) {
        var dynamicList = hook.list.list.querySelector('[data-dynamic]');

        var loadingTemplate = document.createElement('div');
        loadingTemplate.innerHTML = config.loadingTemplate;
        loadingTemplate.setAttribute('data-loading-template', '');

        this.listTemplate = dynamicList.outerHTML;
        dynamicList.outerHTML = loadingTemplate.outerHTML;
      }

      this._loadUrlData(config.endpoint)
        .then(function(d) {
          if (config.loadingTemplate) {
            var dataLoadingTemplate = hook.list.list.querySelector('[data-loading-template]');

            if (dataLoadingTemplate) {
              dataLoadingTemplate.outerHTML = self.listTemplate;
            }
          }
          hook.list[config.method].call(hook.list, d);
        }).catch(function(e) {
          throw new droplabAjaxException(e.message || e);
        });
    },

    destroy: function() {
    }
  };
});