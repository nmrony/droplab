/* global droplab */
require('../window')(function(w){
  w.droplabFilter = {
    keydownWrapper: function(e){
      var list = e.detail.hook.list;
      var data = list.data;
      var value = e.detail.hook.trigger.value.toLowerCase();
      var config = e.detail.hook.config.droplabFilter;
      var matches = [];
      var filterFunction;
      // will only work on dynamically set data
      if(!data){
        return;
      }

      if (config && config.filterFunction && typeof config.filterFunction === 'function') {
        filterFunction = config.filterFunction;
      } else {
        filterFunction = function(o){
          // cheap string search
          o.droplab_hidden = o[config.template].toLowerCase().indexOf(value) === -1;
          return o;
        };
      }

      matches = data.map(function(o) {
        return filterFunction(o, value);
      });
      list.render(matches);
    },

    init: function init(hookInput) {
      var config = hookInput.config.droplabFilter;

      if (!config || (!config.template && !config.filterFunction)) {
        return;
      }

      this.hookInput = hookInput;
      this.hookInput.trigger.addEventListener('keyup.dl', this.keydownWrapper);
    },

    destroy: function destroy(){
      this.hookInput.trigger.removeEventListener('keyup.dl', this.keydownWrapper);
    }
  };
});