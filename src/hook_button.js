var CustomEvent = require('./custom_event_polyfill');
var Hook = require('./hook');

var HookButton = function(trigger, list, plugins, config) {
  Hook.call(this, trigger, list, plugins, config);
  this.type = 'button';
  this.event = 'click';
  this.addEvents();
  this.addPlugins();
};

HookButton.prototype = Object.create(Hook.prototype);

Object.assign(HookButton.prototype, {
  addPlugins: function() {
    for(var i = 0; i < this.plugins.length; i++) {
      this.plugins[i].init(this);
    }
  },

  clicked: function(e){
    var buttonEvent = new CustomEvent('click.dl', {
      detail: {
        hook: this,
      },
      bubbles: true,
      cancelable: true
    });
    this.list.show();
    e.target.dispatchEvent(buttonEvent);
  },

  addEvents: function(){
    this.clickedWrapper = this.clicked.bind(this);
    this.trigger.addEventListener('click', this.clickedWrapper);
  },

  removeEvents: function(){
    this.trigger.removeEventListener('click', this.clickedWrapper);
  },

  restoreInitialState: function() {
    this.list.list.innerHTML = this.list.initialState;
  },

  removePlugins: function() {
    for(var i = 0; i < this.plugins.length; i++) {
      this.plugins[i].destroy();
    }
  },

  destroy: function() {
    this.restoreInitialState();
    this.removeEvents();
    this.removePlugins();
  },


  constructor: HookButton,
});


module.exports = HookButton;
