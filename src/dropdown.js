var CustomEvent = require('./custom_event_polyfill');
var utils = require('./utils');

var DropDown = function(list) {
  this.hidden = true;
  this.list = list;
  this.items = [];
  this.getItems();
  this.initTemplateString();
  this.addEvents();
  this.initialState = list.innerHTML;
};

Object.assign(DropDown.prototype, {
  getItems: function() {
    this.items = [].slice.call(this.list.querySelectorAll('li'));
    return this.items;
  },

  initTemplateString: function() {
    var items = this.items || this.getItems();

    var templateString = '';
    if(items.length > 0) {
      templateString = items[items.length - 1].outerHTML;
    }
    this.templateString = templateString;
    return this.templateString;
  },

  clickEvent: function(e) {
    // climb up the tree to find the LI
    var selected = utils.closest(e.target, 'LI');

    if(selected) {
      e.preventDefault();
      this.hide();
      var listEvent = new CustomEvent('click.dl', {
        detail: {
          list: this,
          selected: selected,
          data: e.target.dataset,
        },
      });
      this.list.dispatchEvent(listEvent);
    }
  },

  addEvents: function() {
    this.clickWrapper = this.clickEvent.bind(this);
    // event delegation.
    this.list.addEventListener('click', this.clickWrapper);
  },

  toggle: function() {
    if(this.hidden) {
      this.show();
    } else {
      this.hide();
    }
  },

  setData: function(data) {
    this.data = data;
    this.render(data);
  },

  addData: function(data) {
    this.data = (this.data || []).concat(data);
    this.render(this.data);
  },

  // call render manually on data;
  render: function(data){
    // debugger
    // empty the list first
    var templateString = this.templateString;
    var newChildren = [];
    var toAppend;

    newChildren = (data ||[]).map(function(dat){
      var html = utils.t(templateString, dat);
      var template = document.createElement('div');
      template.innerHTML = html;

      // Help set the image src template
      var imageTags = template.querySelectorAll('img[data-src]');
      // debugger
      for(var i = 0; i < imageTags.length; i++) {
        var imageTag = imageTags[i];
        imageTag.src = imageTag.getAttribute('data-src');
        imageTag.removeAttribute('data-src');
      }

      if(dat.hasOwnProperty('droplab_hidden') && dat.droplab_hidden){
        template.firstChild.style.display = 'none'
      }else{
        template.firstChild.style.display = 'block';
      }
      return template.firstChild.outerHTML;
    });
    toAppend = this.list.querySelector('ul[data-dynamic]');
    if(toAppend) {
      toAppend.innerHTML = newChildren.join('');
    } else {
      this.list.innerHTML = newChildren.join('');
    }
  },

  show: function() {
    // debugger
    this.list.style.display = 'block';
    this.hidden = false;
  },

  hide: function() {
    // debugger
    this.list.style.display = 'none';
    this.hidden = true;
  },

  destroy: function() {
    this.hide();
    this.list.removeEventListener('click', this.clickWrapper);
  }
});

module.exports = DropDown;
